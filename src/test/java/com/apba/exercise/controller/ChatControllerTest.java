package com.apba.exercise.controller;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.apba.exercise.controller.ChatController;
import com.apba.exercise.model.ChatMessage;
import com.apba.exercise.service.ChatService;


@RunWith(MockitoJUnitRunner.class)
public class ChatControllerTest {

	
	private ChatController controller;
	
	@Mock
	private ChatService chatService;
	
	@Before
	public void setUp(){
		controller = new ChatController(chatService);
	}
	
	
	@Test
	public void shouldSendMessage() {
		ChatMessage chatMessage = this.createMockMessage();
		controller.sendMessage(chatMessage);
		verify(chatService,times(1)).sendMessage(chatMessage);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void sendingNullShouldRaiseAnIllegalArgumentException(){
		controller.sendMessage(null);
	}
	
	@Test
	public void shouldAddUsers() {
		ChatMessage chatMessage = this.createMockMessage();
		controller.addUser(chatMessage, null);
		verify(chatService,times(1)).addUser(chatMessage, null);
	}
	
	private ChatMessage createMockMessage() {
		return new ChatMessage(1, ChatMessage.MessageType.CHAT, "test", "testSender", "testReceiber", false, new Date());
	}

}
