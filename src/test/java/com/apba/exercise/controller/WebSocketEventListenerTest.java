package com.apba.exercise.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.messaging.Message;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
import org.springframework.web.socket.messaging.SessionSubscribeEvent;

import com.apba.exercise.config.StompHeaderAccessorBean;
import com.apba.exercise.controller.WebSocketEventListener;

@RunWith(MockitoJUnitRunner.class)
public class WebSocketEventListenerTest {
	
	
	private WebSocketEventListener webSocketEventListener;
	
	@Mock
	private SimpMessageSendingOperations messagingTemplate;
	
	
	@Mock
	private Message<byte[]> message;
	
	@Mock 
	private StompHeaderAccessorBean stompHeaderAccessorBean;
	
	@Mock
	private StompHeaderAccessor stompHeaderAccessor;
	
	private Map<String,String> usersLogged;
	
	private Map<String, Object> userLoggedSession;
	
	
	private static final String TEST_USERNAME = "testUsername";
	
	
	@Before
	public void setUp() {
		usersLogged = new HashMap<String, String>();
		userLoggedSession = new HashMap<String, Object>();
		webSocketEventListener = new WebSocketEventListener(messagingTemplate, usersLogged, stompHeaderAccessorBean);
	}

	@Test
	public void shouldHandleSubscription() {
		SessionSubscribeEvent event = Mockito.mock(SessionSubscribeEvent.class);

		when(event.getMessage()).thenReturn(message);
		when(stompHeaderAccessorBean.wrap(message)).thenReturn(stompHeaderAccessor);
		when(stompHeaderAccessor.getDestination()).thenReturn("/topic/"+TEST_USERNAME);
		webSocketEventListener.handleSubscription(event);
		
		assertEquals(usersLogged.size(), 1);
	}
	
	@Test
	public void shouldNoHandleSubscriptionWithSubscripbedWithPublicWs() {
		SessionSubscribeEvent event = Mockito.mock(SessionSubscribeEvent.class);

		when(event.getMessage()).thenReturn(message);
		when(stompHeaderAccessorBean.wrap(message)).thenReturn(stompHeaderAccessor);
		when(stompHeaderAccessor.getDestination()).thenReturn("/topic/public");
		webSocketEventListener.handleSubscription(event);
		
		assertEquals(usersLogged.size(), 0);
	}
	
	
	@Test
	public void shouldHandleDisconect() {
		
		
		// A user need to be in our system for disconnection
		userLoggedSession.put("username", TEST_USERNAME);
		
		SessionDisconnectEvent event = Mockito.mock(SessionDisconnectEvent.class);

		when(event.getMessage()).thenReturn(message);
		when(stompHeaderAccessorBean.wrap(message)).thenReturn(stompHeaderAccessor);
		when(stompHeaderAccessor.getSessionAttributes()).thenReturn(userLoggedSession);
		webSocketEventListener.handleWebSocketDisconnectListener(event);
		assertEquals(usersLogged.size(), 0);


	}
	

}
