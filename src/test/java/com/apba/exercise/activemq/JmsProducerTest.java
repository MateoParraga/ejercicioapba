package com.apba.exercise.activemq;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;

import com.apba.exercise.activemq.JmsProducer;
import com.apba.exercise.model.ChatMessage;

@RunWith(MockitoJUnitRunner.class)
public class JmsProducerTest {

	private JmsProducer jmsProducer;
	
	@Mock
	private JmsTemplate jmsTemplate;
	
	@Value("${spring.activemq.queue}")
	String queue;
	
	
	@Before
	public void setUp() {
		jmsProducer = new JmsProducer(jmsTemplate);
	}
	
	@Test
	public void shouldSendMessageToDestination() {
		ChatMessage message = this.craeteMockMessage();
		jmsProducer.send(message);
		verify(jmsTemplate,times(1)).convertAndSend(eq(queue),eq(message));
	}

	
	private ChatMessage craeteMockMessage() {
		return new ChatMessage(1, ChatMessage.MessageType.CHAT, "testContent", "test", "test", false, new Date());
	}
}
