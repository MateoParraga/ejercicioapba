package com.apba.exercise.activemq;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.messaging.simp.SimpMessageSendingOperations;

import com.apba.exercise.activemq.JmsConsumer;
import com.apba.exercise.dao.ChatMessageRepository;
import com.apba.exercise.model.ChatMessage;

@RunWith(MockitoJUnitRunner.class)
public class JmsConsumerTest {

	
	private JmsConsumer jmsConsumer;
	
	@Mock
	private ChatMessageRepository chatMessageRepository;
	@Mock
	private SimpMessageSendingOperations messagingTemplate;
	
	@Before
	public void setUp() {
		jmsConsumer = new JmsConsumer(chatMessageRepository, messagingTemplate);
	}
	
	@Test
	public void test() {
		ChatMessage message = this.createMockMessage();
		jmsConsumer.receive(message);
		verify(chatMessageRepository,times(1)).save(message);
		verify(messagingTemplate,times(1)).convertAndSend(eq("/topic/action-monitor"), Mockito.any(ChatMessage.class));
	}
	
	private ChatMessage createMockMessage() {
		return new ChatMessage(1, ChatMessage.MessageType.CHAT, "testContent", "test", "test", false, new Date());
	}

}
