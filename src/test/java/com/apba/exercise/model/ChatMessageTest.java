package com.apba.exercise.model;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;

import com.apba.exercise.model.ChatMessage;

public class ChatMessageTest {

	ChatMessage chatMessage;
	
	@Test
	public void shouldCreateByFullConstructor() {
		Date dateTest = new Date();
		chatMessage = new ChatMessage(1, ChatMessage.MessageType.CHAT, "testContent", "testSender", "testReceiver", false, dateTest);
	
		assertEquals(1, chatMessage.getId());
		assertEquals(ChatMessage.MessageType.CHAT, chatMessage.getType());
		assertEquals("testContent", chatMessage.getContent());
		assertEquals("testReceiver", chatMessage.getReceiver());
		assertEquals("testSender", chatMessage.getSender());
		assertEquals("testReceiver", chatMessage.getReceiver());
		assertEquals(false, chatMessage.isShowed());
		assertEquals(dateTest, chatMessage.getTimestamp());
	}
	
	@Test
	public void shouldCreateAndSetter() {
		Date dateTest = new Date();
		chatMessage = new ChatMessage();
		chatMessage.setId(1);
		chatMessage.setType(ChatMessage.MessageType.CHAT);
		chatMessage.setContent("testContent");
		chatMessage.setSender("testSender");
		chatMessage.setReceiver("testReceiver");
		chatMessage.setShowed(false);
		chatMessage.setTimestamp(dateTest);
		
		assertEquals(1, chatMessage.getId());
		assertEquals(ChatMessage.MessageType.CHAT, chatMessage.getType());
		assertEquals("testContent", chatMessage.getContent());
		assertEquals("testReceiver", chatMessage.getReceiver());
		assertEquals("testSender", chatMessage.getSender());
		assertEquals("testReceiver", chatMessage.getReceiver());
		assertEquals(false, chatMessage.isShowed());
		assertEquals(dateTest, chatMessage.getTimestamp());
		
	}
	
	@Test
	public void shouldPrintProperly() {
		Date timestamp = new Date();
		int id = 1;
		chatMessage = new ChatMessage(1, ChatMessage.MessageType.CHAT, "testContent", "testSender", "testReceiver", false, timestamp);
		assertEquals("timestamp=" + timestamp +
                ", a row with ID = " + id + 
                ", was inserted", chatMessage.toString());
	}
	

}
