package com.apba.exercise.service;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageSendingOperations;

import com.apba.exercise.activemq.JmsProducer;
import com.apba.exercise.dao.ChatMessageRepository;
import com.apba.exercise.model.ChatMessage;
import com.apba.exercise.service.ChatService;

@RunWith(MockitoJUnitRunner.class)
public class ChatServiceTest {

	
	private static final String TEST_SENDER = "testSender";
	private static final String TEST_RECEIVER = "testReceiver";

	private ChatService chatService;
	
	@Mock
	private SimpMessageSendingOperations messagingTemplate;
	@Mock
	private JmsProducer jmsProducer;
	@Mock
	private ChatMessageRepository chatMessageRepository;
	@Mock
	private SimpMessageHeaderAccessor headerAccessor;
	
	private Map<String,String> usersLogged;
	
	@Before
	public void setUp(){
		usersLogged = new HashMap<String, String>();
		usersLogged.put("123456", TEST_RECEIVER);
		usersLogged.put("123457", TEST_SENDER);
		chatService = new ChatService(messagingTemplate, jmsProducer, chatMessageRepository, usersLogged);
	}
	
	
	
	@Test
	public void shouldSendMessage() {
		ChatMessage chatMessage = this.createMockMessage();
		chatService.sendMessage(chatMessage);
		verify(messagingTemplate,times(1)).convertAndSend(eq("/topic/"+TEST_RECEIVER),eq(chatMessage));
		verify(messagingTemplate,times(1)).convertAndSend(eq("/topic/"+TEST_SENDER),eq(chatMessage));
		verify(jmsProducer,times(1)).send(eq(chatMessage));
	}
	
	@Test
	public void shouldAddUsers() {
		
		String newUser = "testNewSender";
		ChatMessage chatMessage = this.createMockMessage();
		chatMessage.setSender(newUser);
		chatService.addUser(chatMessage, headerAccessor);
		verify(messagingTemplate,times(1)).convertAndSend(eq("/topic/public"),eq(chatMessage));
		// I finally show to the new user the user logged
		verify(messagingTemplate,times(2)).convertAndSend(eq("/topic/"+newUser), Mockito.any(ChatMessage.class));
		
		
	}


	private ChatMessage createMockMessage() {
		return new ChatMessage(1, ChatMessage.MessageType.CHAT, "testContent", TEST_SENDER, TEST_RECEIVER, false, new Date());
	}
	
	
	

}
