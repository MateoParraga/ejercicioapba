package com.apba.exercise.service;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Service;

import com.apba.exercise.activemq.JmsProducer;
import com.apba.exercise.dao.ChatMessageRepository;
import com.apba.exercise.model.ChatMessage;


/**
 * Main logic for the chat
 * @author Mateo
 *
 */
@Service
public class ChatService {

	private static final Logger log = LoggerFactory.getLogger(ChatService.class);

	/**
	 * WebSocket template
	 */
	private SimpMessageSendingOperations messagingTemplate;
	/**
	 * Active mq communication producer
	 */
	private JmsProducer jmsProducer;
	/**
	 * Repository for accessing to database
	 */
	private ChatMessageRepository chatMessageRepository;
	/**
	 * In memory user logged in our chat
	 */
	private Map<String,String> usersLogged;
	
	
	/**
	 * Service constructor
	 * 
	 * @param messagingTemplate
	 * @param jmsProducer
	 * @param chatMessageRepository
	 */
	public ChatService(SimpMessageSendingOperations messagingTemplate, JmsProducer jmsProducer, ChatMessageRepository chatMessageRepository, Map<String,String> usersLogged) {
		super();
		this.messagingTemplate = messagingTemplate;
		this.jmsProducer = jmsProducer;
		this.chatMessageRepository = chatMessageRepository;
		this.usersLogged = usersLogged;
	}

	/**
	 * All the logic needed for send the message
	 * 
	 * @param chatMessage
	 */
	public void sendMessage(ChatMessage chatMessage) {
		
		
		// Send message to private topic for the receiver user
		if(usersLogged.containsValue(chatMessage.getReceiver())){
			log.info("Message has been showed to user " + chatMessage.getReceiver());
			messagingTemplate.convertAndSend("/topic/" + chatMessage.getReceiver(), chatMessage);
			chatMessage.setShowed(true);
		}

		// Send message to myself for see my own message
		if(!chatMessage.getSender().equals(chatMessage.getReceiver())) {
			messagingTemplate.convertAndSend("/topic/"+chatMessage.getSender(), chatMessage); 
		}
		// Using the queue for save the message and show in the action monitor queue id and timestamp for the message
		jmsProducer.send(chatMessage);

	}

	


	/**
	 * Register a username in our chat 
	 * @param chatMessage
	 * @param headerAccessor
	 */
	public void addUser(ChatMessage chatMessage, SimpMessageHeaderAccessor headerAccessor) {
    	String username = chatMessage.getSender();
		// Add username in web socket session
		headerAccessor.getSessionAttributes().put("username", username);
		// This message only say username join to chat for identify the user that you can chat
		// it will be showed in all the sessions
		messagingTemplate.convertAndSend("/topic/public", chatMessage); 
		// Here we need to show to the new user all the users logged in our chat
		for (Map.Entry<String, String> entry : usersLogged.entrySet()) {
			if (!entry.getValue().equals(username)) {
				messagingTemplate.convertAndSend("/topic/"+chatMessage.getSender(), this.joinMessage(entry.getValue()));
			}
		}
		// User registered see message not showed
		this.retrieveMessageNotShowed(chatMessage.getSender());
		
	}
	
	/**
	 * Send messages not showed to the user when he is registering again
	 * @param username
	 */
	private void retrieveMessageNotShowed(String username) {
		
		List<ChatMessage> chatMessageList = chatMessageRepository.findByReceiverAndShowedFalse(username);
		
		for (Iterator<ChatMessage> iterator = chatMessageList.iterator(); iterator.hasNext();) {
			ChatMessage chatMessage = (ChatMessage) iterator.next();
			messagingTemplate.convertAndSend("/topic/"+username, chatMessage);
			chatMessage.setShowed(true);
			chatMessageRepository.save(chatMessage);
		}
		
	}
	
	/**
	 * Simple message for identify that a new user was registered
	 * @param username
	 * @return
	 */
	private ChatMessage joinMessage(String username) {
		ChatMessage chatMessage = new ChatMessage();
		chatMessage.setType(ChatMessage.MessageType.JOIN);
		chatMessage.setSender(username);
		return chatMessage;
	}


}
