package com.apba.exercise.config;


import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

 	
/**
 * This class is going to be used for defining bean that spring need to instance 
 * @author Mateo
 *
 */
@Configuration
public class BeanConfig {
 
	  /** 	
	   * In memory all user registered in chat
	   * @return
	   */
	  @Bean
	  public Map<String,String> usersLogged(){
			return new HashMap<>();
	  }
		
	
}
