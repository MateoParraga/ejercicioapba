package com.apba.exercise.config;

import org.springframework.messaging.Message;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;

/**
 * Wrap Static method for testing purpose
 * @author Mateo
 *
 */
@Component
public class StompHeaderAccessorBean {

	public StompHeaderAccessor wrap(Message<byte[]> message) {
		return StompHeaderAccessor.wrap(message);
	}
	
}
