package com.apba.exercise.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.apba.exercise.model.ChatMessage;

/**
 * Repository class for CRUD methods and additional functionality
 * @author Mateo
 *
 */
public interface ChatMessageRepository extends CrudRepository<ChatMessage, Long> {

	public List<ChatMessage> findByReceiverAndShowedFalse(String receiver);

	
	
}
