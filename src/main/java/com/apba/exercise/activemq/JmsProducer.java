package com.apba.exercise.activemq;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.apba.exercise.model.ChatMessage;



/**
 * Produce a message to the queue
 * 
 * @author Mateo
 *
 */
@Component
public class JmsProducer {
	
  JmsTemplate jmsTemplate;
  
  @Value("${spring.activemq.queue}")
  String queue;
  
  public JmsProducer(JmsTemplate jmsTemplate) {
	  super();
	  this.jmsTemplate = jmsTemplate;
  }
  
  
  /**
   * Produce send chatMessage to message queue
   * @param chatMessage
   */
  public void send(ChatMessage chatMessage){
    jmsTemplate.convertAndSend(queue, chatMessage);
  }
  
}