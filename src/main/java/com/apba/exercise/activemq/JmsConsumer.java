package com.apba.exercise.activemq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Component;

import com.apba.exercise.controller.ChatController;
import com.apba.exercise.dao.ChatMessageRepository;
import com.apba.exercise.model.ChatMessage;

/**
 * Using Consumer for save message and show in the action monitor view the message info
 * like in the test example, contains the destination of our messages
 * 
 * @author Mateo
 *
 */
@Component
public class JmsConsumer {

	private static final Logger log = LoggerFactory.getLogger(ChatController.class);

	/**
	 * Repository for accessing to database
	 */
	private ChatMessageRepository chatMessageRepository;
	/**
	 * WebSocket template
	 */
	private SimpMessageSendingOperations messagingTemplate;
	
	
	
	public JmsConsumer(ChatMessageRepository chatMessageRepository, SimpMessageSendingOperations messagingTemplate) {
		super();
		this.chatMessageRepository = chatMessageRepository;
		this.messagingTemplate = messagingTemplate;
	}

	
	@JmsListener(destination = "${spring.activemq.queue}", containerFactory = "jsaFactory")
	public void receive(ChatMessage message) {
		log.info("MESSAGE RECEIVED");
		chatMessageRepository.save(message);
		message.setContent(message.toString());
		messagingTemplate.convertAndSend("/topic/action-monitor", message);
		log.info(message.toString());
	}

}