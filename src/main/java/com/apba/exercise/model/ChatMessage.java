package com.apba.exercise.model;

import java.util.Date;

import javax.persistence.*;

/**
 * Entity 
 * @author Mateo
 *
 */
@Entity
@Table
public class ChatMessage {
	
	@Id
    @GeneratedValue()
    private long id;
	
	private MessageType type;
	
	private String content;
	
	private String sender;
	
	private String receiver;
	
	private boolean showed;
	
	protected Date timestamp;
	
	public enum MessageType {
        CHAT,
        JOIN,
        LEAVE
    }
	
	public ChatMessage() {
		
	}
	
	public ChatMessage(long id, MessageType type, String content, String sender, String receiver, boolean showed,
			Date timestamp) {
		super();
		this.id = id;
		this.type = type;
		this.content = content;
		this.sender = sender;
		this.receiver = receiver;
		this.showed = showed;
		this.timestamp = timestamp;
	}

	@PrePersist
	protected void onCreate() {
		timestamp = new Date();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public MessageType getType() {
		return type;
	}

	public void setType(MessageType type) {
		this.type = type;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public boolean isShowed() {
		return showed;
	}

	public void setShowed(boolean showed) {
		this.showed = showed;
	}
	
	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	@Override
    public String toString() {
        return  "timestamp=" + timestamp +
                ", a row with ID = " + id + 
                ", was inserted";
    }
	
}
