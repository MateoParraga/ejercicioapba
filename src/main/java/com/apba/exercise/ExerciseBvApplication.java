package com.apba.exercise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Starter
 * @author Mateo test
 *
 */
@SpringBootApplication
public class ExerciseBvApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExerciseBvApplication.class, args);
	}

}
