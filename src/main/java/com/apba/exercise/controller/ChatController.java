package com.apba.exercise.controller;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;

import com.apba.exercise.model.ChatMessage;
import com.apba.exercise.service.ChatService;

/**
 * Controller, it defines endpoints for establishment the communication
 * 
 * @author Mateo
 *
 */
@Controller
public class ChatController {
	
	/**
	 * Service
	 */
	private ChatService chatService;
	
	
	public ChatController(ChatService chatService) {
		super();
		this.chatService = chatService;
	}
	
    @MessageMapping("/chat.sendMessage")
    public void sendMessage(@Payload ChatMessage chatMessage) {
    	if (chatMessage == null || chatMessage.getReceiver() == null || chatMessage.getSender() == null || chatMessage.getContent() == null ||
    			chatMessage.getReceiver().isEmpty() || chatMessage.getSender().isEmpty() || chatMessage.getContent().isEmpty()) {
			throw new IllegalArgumentException("Incorrect Message");
    	}
    	chatService.sendMessage(chatMessage);
    }

    @MessageMapping("/chat.addUser")
    public void addUser(@Payload ChatMessage chatMessage, 
                               SimpMessageHeaderAccessor headerAccessor) {
    	chatService.addUser(chatMessage, headerAccessor);
    }

    
    

}
