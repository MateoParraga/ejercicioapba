package com.apba.exercise.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
import org.springframework.web.socket.messaging.SessionSubscribeEvent;

import com.apba.exercise.config.StompHeaderAccessorBean;
import com.apba.exercise.model.ChatMessage;

/**
 * This class listen the different incoming events that we need to control (subscription websocket, disconnection to websocket)
 * 
 * @author Mateo
 *
 */
@Component
public class WebSocketEventListener {

	private static final String ACTION_MONITOR_WS = "action-monitor";

	private static final String PUBLIC_WS = "public";

	private static final Logger logger = LoggerFactory.getLogger(WebSocketEventListener.class);

	/**
	 * WebSocket template
	 */
    private SimpMessageSendingOperations messagingTemplate;
    /**
     * Logged user in memory
     */
    private Map<String,String> usersLogged;

    /**
     * Instance from stomp header accessor
     */
	private StompHeaderAccessorBean stompHeaderAccessorBean;

    
    
    public WebSocketEventListener(SimpMessageSendingOperations messagingTemplate, Map<String,String> usersLogged, StompHeaderAccessorBean stompHeaderAccessorBean) {
    	super();
		this.messagingTemplate = messagingTemplate;
		this.usersLogged = usersLogged;
		this.stompHeaderAccessorBean = stompHeaderAccessorBean;
	}
    
    
    
    /* Event not needed
	@EventListener
	public void handleWebSocketConnectListener(SessionConnectedEvent event) {
		logger.info("Listener Received a new web socket connection event");
	} */
	
	
	@EventListener
	public void handleSubscription(SessionSubscribeEvent event){
		logger.info("Listener Received a new web socket subscription event");
		StompHeaderAccessor accessor = stompHeaderAccessorBean.wrap(event.getMessage());
		String username = accessor.getDestination().replaceAll("/topic/", "");
		if (username != null && !username.isEmpty() && !username.equals(PUBLIC_WS) && !username.equals(ACTION_MONITOR_WS)) {
			usersLogged.put(accessor.getSessionId(), username);
		}
	}
	
	

    @EventListener
    public void handleWebSocketDisconnectListener(SessionDisconnectEvent event) {
    	logger.info("Listener Received a new web socket disconection event");
        StompHeaderAccessor headerAccessor = stompHeaderAccessorBean.wrap(event.getMessage());

        String username = (String) headerAccessor.getSessionAttributes().get("username");
        if(username != null) {
            logger.info("User Disconnected : " + username);
            usersLogged.remove(headerAccessor.getSessionId());
            ChatMessage chatMessage = leaveMessage(username);
            // Send to public informing user leave the chat room
            messagingTemplate.convertAndSend("/topic/public", chatMessage);
        }
    }

	private ChatMessage leaveMessage(String username) {
		ChatMessage chatMessage = new ChatMessage();
		chatMessage.setType(ChatMessage.MessageType.LEAVE);
		chatMessage.setSender(username);
		return chatMessage;
	}
}
