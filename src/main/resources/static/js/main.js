'use strict';

var usernamePage = document.querySelector('#username-page');
var chatPage = document.querySelector('#chat-page');
var usernameForm = document.querySelector('#usernameForm');
var messageForm = document.querySelector('#messageForm');
var messageInput = document.querySelector('#message');
var receiverInput = document.querySelector('#receiver');
var messageArea = document.querySelector('#messageArea');
var connectingElement = document.querySelector('.connecting');
var actionMonitorPage = document.querySelector('#actionMonitor-page');
var messageActionMonitorArea = document.querySelector('#messageActionMonitorArea');
var connectingElementActionMonitor =  document.querySelector('.connecting-actionMonitor');

var stompClient = null;
var username = null;

var colors = [
    '#2196F3', '#32c787', '#00BCD4', '#ff5652',
    '#ffc107', '#ff85af', '#FF9800', '#39bbb0'
];

function connect(event) {
    username = document.querySelector('#name').value.trim();

    if(username) {
        usernamePage.classList.add('hidden');
        chatPage.classList.remove('hidden');

        var socket = new SockJS('/ws');
        stompClient = Stomp.over(socket);

        stompClient.connect({}, onConnected, onError);
    }
    event.preventDefault();
}


function onConnected() {
    // Subscribe to the Public Topic, show public message for joining and leaving users
    stompClient.subscribe('/topic/public', onMessageReceived);
    // Subscribe to the Private Topic for the user logged
    stompClient.subscribe('/topic/'.concat(username), onMessageReceived);
    // Subscribe to action monitor public queue
    stompClient.subscribe('/topic/action-monitor', onMessageReceivedActionMonitor);
    

    // Tell your username to the server
    stompClient.send("/app/chat.addUser",
        {},
        JSON.stringify({sender: username, type: 'JOIN'})
    )

    connectingElement.classList.add('hidden');
    connectingElementActionMonitor.classList.add('hidden');
}


function onError(error) {
    connectingElement.textContent = 'Could not connect to WebSocket server. Please refresh this page to try again!';
    connectingElement.style.color = 'red';
    connectingElementActionMonitor.textContent = 'Could not connect to WebSocket server. Please refresh this page to try again!';
    connectingElementActionMonitor.style.color = 'red';
}

function showActionMonitor(event) {
	usernamePage.classList.add('hidden');
    chatPage.classList.add('hidden');
    actionMonitorPage.classList.remove('hidden');  
}

function showChatRoom(event) {
	usernamePage.classList.add('hidden');
    chatPage.classList.remove('hidden');
    actionMonitorPage.classList.add('hidden'); 
}




function sendMessage(event) {
    var messageContent = messageInput.value.trim();
    if(messageContent && stompClient) {
        var chatMessage = {
            sender: username,
            receiver: receiverInput.value,
            content: messageInput.value,
            type: 'CHAT'
        };
        stompClient.send("/app/chat.sendMessage", {}, JSON.stringify(chatMessage));
        messageInput.value = '';
    }
    event.preventDefault();
}


function onMessageReceived(payload) {
    var message = JSON.parse(payload.body);

    var messageElement = document.createElement('li');

    if(message.type === 'JOIN') {
        messageElement.classList.add('event-message');
        message.content = message.sender + ' joined!';
    } else if (message.type === 'LEAVE') {
        messageElement.classList.add('event-message');
        message.content = message.sender + ' left!';
    } else {
        messageElement.classList.add('chat-message');

        var avatarElement = document.createElement('i');
        var avatarText = document.createTextNode(message.sender[0]);
        avatarElement.appendChild(avatarText);
        avatarElement.style['background-color'] = getAvatarColor(message.sender);

        messageElement.appendChild(avatarElement);

        var usernameElement = document.createElement('span');
        var usernameText = document.createTextNode(message.sender);
        usernameElement.appendChild(usernameText);
        messageElement.appendChild(usernameElement);
    }

    var textElement = document.createElement('p');
    var messageText = document.createTextNode(message.content);
    textElement.appendChild(messageText);

    messageElement.appendChild(textElement);

    messageArea.appendChild(messageElement);
    messageArea.scrollTop = messageArea.scrollHeight;
}


function onMessageReceivedActionMonitor(payload) {
    var message = JSON.parse(payload.body);

    var messageElement = document.createElement('li');
    
    messageElement.classList.add('chat-message');

    var textElement = document.createElement('p');
    var messageText = document.createTextNode(message.content);
    textElement.appendChild(messageText);

    messageElement.appendChild(textElement);

    messageActionMonitorArea.appendChild(messageElement);
    messageActionMonitorArea.scrollTop = messageActionMonitorArea.scrollHeight;
}


function getAvatarColor(messageSender) {
    var hash = 0;
    for (var i = 0; i < messageSender.length; i++) {
        hash = 31 * hash + messageSender.charCodeAt(i);
    }
    var index = Math.abs(hash % colors.length);
    return colors[index];
}

usernameForm.addEventListener('submit', connect, true)
messageForm.addEventListener('submit', sendMessage, true)