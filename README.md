# Chat Room and Action Monitor App for APBA

This is a sample Java 1.8 / Maven / Spring Boot (version 2.1.4) application.

## How to Run 

This application is packaged as a jar which has Tomcat 8 embedded. No Tomcat or JBoss installation is necessary. It uses spring boot functionality.
No Active mq installation is needed in it using a embedded activemq to facilitate the project execution 



* Clone this repository 
* Make sure you are using JDK 1.8 and Maven 3.x
* You can build the project and run the tests by running ```mvn clean install```
* Once successfully built, you can run the service:
	- Type mvn spring-boot:run
	- In your favourite IDE Run starter class com.apba.exerciseBV.ExerciseBvApplication as Java Application
* Check the stdout or action_monitor.log file in main folder



## About the Service

This Application is a chat room where the user can see other users connected and they can communicate with each other in a easy way. The application contains a view with information about all incoming messages (message id and time) which is accessible for all users.

All the junit test was done using mockito and trying to include the maximun coverage as possible.


### Get information about aplication, system health, configurations, etc.

* http://localhost:8080
	Main page with all the functionality requested, chat room and active monitor
* http://localhost:8080/actuator/health 
	Health/Status check if application is up and working fine.
* http://localhost:8080/actuator/info 
	Info/Version provide information about the application.
* http://localhost:8080/console/
	H2 in memory database webconsole
	
### In memory database info	

	url=jdbc:h2:mem:apba;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
	url=jdbc:h2:mem:apba;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
	platform=h2
	username = sa
	password =
	driverClassName = org.h2.Driver
	database-platform=org.hibernate.dialect.H2Dialect



